# impafud - Infinite Monkeys Pissed About Fake USB Drives

## What it does
`impafud` outputs a deterministic stream of pseudo-random data generated from a passphrase, and writes it sequentially in a block device. Subsequently, the integrity of the device can be checked by running the same process and, instead of writing, comparing with the device contents. This way, fake or faulty storage devices can be tested with maximum reliability, and no way for the fake circuitry in counterfeit devices to circumvent.

## Why it does
Because everybody, especially the infinite monkeys (who need reliable media to store their attempts at writing Shakespearean plays), is royally pissed with counterfeit storage devices that make you lose your data.

## How it does
A initial block (you could call it the minus-oneth block) is generated with some predetermined content, plus a 32-byte passphrase. Block 0 is made by encrypting block -1 with the passphrase. Block 1 is made by encrypting block 0 with the passphrase. Block 2 is made by encrypting block 1 with the passphrase. And so on and so forth, until the write operation tells us there's no space left on the device, at which point it reports how many megabytes it wrote. This number _should_ be close to the advertised capacity.

Before removing the media, you should wait for the program to finish - it will only do so when all blocks are written to the device.

Ideally, you should do the verification step in other machine with the same version of `impafud` installed. Or, at the very least, turn your machine off and on again, and attach the drive to a different USB port.

## Aren't you being _just_ a bit too paranoid?
Yes. Never regretted it.

## Installation
For now, just `make` and copy the `impafud` binary to wherever you like, for example `/usr/local/bin`. It was developed and tested on Linux Mint 21.2 (which is essentially Ubuntu 22.04 LTS).

## Usage

`impafud RememberNovember7 /dev/sda`

Fills the device in /dev/sda with a pseudo-random pattern based on the passphrase "RememberNovember7"

`impafud RememberNovember7 /dev/sda -v`

Verifies a device previously filled by impafud using that passphrase. Any mismatch will be displayed and the process stopped.

`impafud -p /dev/sda`

Fills the device, but instead of using a provided passphrase, generated a random one and displays it for the subsequent verification.

## Contributing
For now, if you have suggestions, just drop me an e-mail. Good suggestions will make the author happy. Good suggestions in the form of patches will make the author even happier.

## License
This software is Copyright (C)2024 by Juan Carlos Castro y Castro and is licensed under the terms of the GNU General Public License 2.0, which should be included if you obtained this in source form.
