# To Do

- When outputting a block mismatch, mark in which line(s) the mismatch(es) happened.
- Stop malloc()'ing like crazy in nextblock() and use two alternating static blocks.
