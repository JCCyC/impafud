#include <openssl/sha.h>
#include <stdio.h>
#include <string.h>

/* Used for nothing for now, just keeping this code because it might be useful */

int main() {

  unsigned char data[] = "some text";
  unsigned char hash[SHA512_DIGEST_LENGTH];
  SHA512(data, strlen((char *)data), hash);

  for (int i = 0; i < SHA512_DIGEST_LENGTH; i++)
    printf("%02x", hash[i]);
  putchar('\n');
}
