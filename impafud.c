/*
 * This file is Copyright (c) 2024 Juan Carlos Castro y Castro.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 2, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <libgen.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <sys/sysmacros.h>
#include <mntent.h>
#include <openssl/aes.h>
#include <openssl/rand.h>

#define SECSIZE 512

static unsigned char passable[] = "?#-+.23456789ABCDEFGHIJKLMNPQRSTUVWXYZ_abcdefghijkmnopqrstuvwxyz";

static void hex_print(const void *pv, size_t len)
{
	const unsigned char *p = pv;
	unsigned char asciirender[34] = "................................";

	if (!pv)
		printf("NULL\n");
	else
	{
		for (size_t i = 0; i < len; i++)
		{
			int withinline = i & 0x1F;
			unsigned char c = *p++;
			printf("%02X ", c);
			if (c < 32 || c > 126)
				c = '.';
			asciirender[withinline] = c;
			if ((!((i + 1) & 0x1F)) || (i == len - 1))
				printf("%s\n", asciirender);
		}
	}
	printf("\n");
}

unsigned char *nextblock(const unsigned char *block, size_t inputslength, const char *passphrase)
{
	int keylength = 256;

	// 32-byte buffer with passphrase and completed with 0xA5
	unsigned char aes_key[keylength / 8];
	memset(aes_key, 0x5A, keylength / 8);
	strncpy(aes_key, passphrase, keylength / 8);

	// Init vector
	unsigned char iv_enc[AES_BLOCK_SIZE], iv_dec[AES_BLOCK_SIZE];
	memset(iv_enc, 0xA5, sizeof(iv_enc));
	memset(iv_dec, 0xA5, sizeof(iv_dec));

	// Buffer for encrypted sector
	const size_t encslength = ((inputslength + AES_BLOCK_SIZE) / AES_BLOCK_SIZE) * AES_BLOCK_SIZE;
	unsigned char enc_out[encslength];
	memset(enc_out, 0, sizeof(enc_out));

	// Do encryption
	AES_KEY enc_key;
	AES_set_encrypt_key(aes_key, keylength, &enc_key);
	AES_cbc_encrypt(block, enc_out, inputslength, &enc_key, iv_enc, AES_ENCRYPT);

	// Return significant bytes
	unsigned char *nextblk = malloc(inputslength);
	if (nextblk)
	{
		memcpy(nextblk, enc_out, inputslength);
	}

	return nextblk;
}

int main(int argc, char **argv)
{
	int keylength = 256, devfd = -254, openflags = O_RDWR;
	char *progname = basename(argv[0]);
	unsigned int majorid = -1, minorid = -1;
	int yesno = 0;
	struct stat fileinfo;
	char passphrase[32];
	unsigned char onesector[SECSIZE];
	char *operation = "Writing... ";

	memset(passphrase, 0, sizeof(passphrase));
	if (argc == 3)
	{
		if (!strcmp(argv[1], "-p"))
		{
			double randnum;
			struct drand48_data randbuf;

			srand48_r(((unsigned long)(time(NULL)) << 16) ^ getpid(), &randbuf);
			for (int i = 0; i < sizeof(passphrase) - 1; i++)
			{
				drand48_r(&randbuf, &randnum);
				passphrase[i] = passable[(int)(randnum * (sizeof(passable) - 1))];
			}
			passphrase[sizeof(passphrase) - 1] = '\0';
		}
		else
		{
			strncpy(passphrase, argv[1], sizeof(passphrase) - 1);
		}
	}
	else if (argc == 4)
	{
		if (argc > 3 && !strcmp(argv[3], "-v"))
		{
			openflags = O_RDONLY;
			operation = "Checking... ";
		}
		if (strlen(argv[1]) < 8)
		{
			fprintf(stderr, "Passphrase too short - minimum 8 characters\n");
			return -2;
		}
		strncpy(passphrase, argv[1], sizeof(passphrase) - 1);
	}
	else
	{
		fprintf(stderr, "Syntax: %s [<passphrase> | -p] <blockdevice> [-v]\n", progname);
		return -1;
	}

	devfd = open(argv[2], openflags);
	if (devfd < 0)
	{
		fprintf(stderr, "Can't open '%s': %s\n", argv[2], strerror(errno));
		return -3;
	}

	// Check if block device and not a partition
	if (stat(argv[2], &fileinfo))
	{
		fprintf(stderr, "Can't obtain info on '%s': %s\n", argv[2], strerror(errno));
		return -4;
	}
	if (!S_ISBLK(fileinfo.st_mode))
	{
		fprintf(stderr, "Can't operate on '%s': not a block device\n", argv[2]);
		return -5;
	}
	majorid = major(fileinfo.st_rdev);
	minorid = minor(fileinfo.st_rdev);
	printf("Major: %d, Minor: %d\n", majorid, minorid);
	if (minor(fileinfo.st_rdev) != 0)
	{
		fprintf(stderr, "Can't operate on '%s' (partition %u), only on an entire block device\n", argv[2], minor(fileinfo.st_rdev));
		return -11;
	}

	if (openflags == O_RDWR)
	{
		printf("The testing procedure will COMPLETELY AND IRREVOCABLY DESTROY any data\n");
		printf("currently recorded on the device. Say Y if you are ABSOLUTELY SURE\n");
		printf("this is indeed your intention: ");
		fflush(stdin);
		yesno = fgetc(stdin);
		fflush(stdin);
		if ((char)yesno != 'Y')
		{
			fprintf(stderr, "Aborted by user\n");
			return -128;
		}
		printf("Passphrase: '%s' (save this for verification phase!)\n", passphrase);
	}

	// Check if mounted
	FILE *mountfd = setmntent("/proc/mounts", "r");
	struct mntent *ent;
	if (mountfd == NULL)
	{
		fprintf(stderr, "Can't obtain info on mounted devices: %s\n", strerror(errno));
		return -4;
	}
	while ((ent = getmntent(mountfd)) != NULL)
	{
		char *devname = ent->mnt_fsname;
		if (devname[0] == '/')
		{
			if (stat(devname, &fileinfo))
			{
				fprintf(stderr, "Unexpected: can't obtain info on mounted fs '%s': %s\n", devname, strerror(errno));
				return -6;
			}
			if (major(fileinfo.st_rdev) == majorid)
			{
				fprintf(stderr, "Device '%s' has a mounted partition at '%s'\n", argv[2], ent->mnt_dir);
				return -7;
			}
		}
	}

	memset(onesector, 0xA5, SECSIZE);
	memcpy(onesector, passphrase, sizeof(passphrase));

	unsigned char *nextblk = nextblock(onesector, SECSIZE, passphrase);
	unsigned char *newnextblk;
	unsigned long nsec = 0;
	printf("%s", operation);
	fflush(stdout);
	time_t starttime = time(NULL);
	while (nextblk)
	{
		if (openflags == O_RDWR)
		{ // Filling: write to device
			if (write(devfd, nextblk, SECSIZE) != SECSIZE)
			{
				int thiserror = errno;

				if (errno == ENOSPC)
				{
					printf("Total %.2lf MiB. Finishing writes... ", ((double)nsec) / 2048);
					close(devfd);
					sync();
					printf("Done\n");
					return 0;
				}
				else
				{
					fprintf(stderr, "Can't write to '%s': %s\n", argv[2], strerror(errno));
					return -8;
				}
			}
		}
		else
		{ // Verifying: compare with device contents
			int bytesread = read(devfd, onesector, SECSIZE);

			if (bytesread < 0)
			{
				fprintf(stderr, "Error at %.2lf MiB reading from '%s': %s\n", ((double)nsec) / 2048, argv[2], strerror(errno));
				return -9;
			}
			else if (bytesread < SECSIZE)
			{
				printf("Successfully tested %.2lf MiB\n", ((double)nsec) / 2048);
				return 0;
			}
			else if (memcmp(onesector, nextblk, SECSIZE))
			{
				fprintf(stderr, "Data mismatch at %.2lf MiB\n", ((double)nsec) / 2048);
				printf("Sector %lu Expected:\n", nsec);
				hex_print(nextblk, SECSIZE);
				printf("Sector %lu Contents:\n", nsec);
				hex_print(onesector, SECSIZE);
				return -10;
			}
		}
		time_t now = time(NULL);
		if (now >= starttime + 60)
		{
			printf("%.2lf MiB... ", ((double)nsec) / 2048);
			fflush(stdout);
			starttime = now;
		}

		newnextblk = nextblock(nextblk, SECSIZE, passphrase);

		free(nextblk);
		nextblk = newnextblk;
		nsec++;
	}

	return 0;
}
